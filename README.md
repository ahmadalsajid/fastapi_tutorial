## FastAPI

Step 1: create virtual environment
```
virtualenv -p python3.8 venv
```
Step 2: install dependencies
```
pip install -r requirements.txt
```
Ste 3: run application
```
uvicorn main:app --reload
```

